import filecmp
import unittest

import numpy as np

from caller_utils import save_array_as_bedgraph
from coverage_expected import cov_s, calculate_coverage
from datatypes import BedpeRecord


class TestCoverageExpected(unittest.TestCase):
    def test_cov_s_1(self):
        cov = np.zeros(10)
        s_len = 4
        actual_result = cov_s(cov, s_len, 1)
        expected_result = np.array([1, 2, 3, 4, 4, 4, 4, 3, 2, 1], dtype=np.float) / (10 - s_len + 1)
        self.assertTrue(np.allclose(expected_result, actual_result))

    def test_cov_s_2(self):
        cov = np.zeros(10)
        s_len = 3
        actual_result = cov_s(cov, s_len, 1)
        expected_result = np.array([1, 2, 3, 3, 3, 3, 3, 3, 2, 1], dtype=np.float) / (10 - s_len + 1)
        self.assertTrue(np.allclose(expected_result, actual_result))

    def test_calculate_coverage(self):
        records = [BedpeRecord('chr1', 3, 4, 'chr1', 5, 6, 1),
                   BedpeRecord('chr1', 4, 6, 'chr1', 8, 9, 1)]
        expected_result = np.array([1., 2., 3., 4., 4., 4., 4., 3., 2., 1.]) / (10 - 4 + 1) + \
                          np.array([1., 2., 3., 3., 3., 3., 3., 3., 2., 1.]) / (10 - 3 + 1)
        actual_result = calculate_coverage(10, records, 1)
        self.assertTrue(np.allclose(expected_result, actual_result))

    def test_save_as_bedgraph_1(self):
        array = np.array([2, 4, 6, 7, 7, 7, 7, 6, 4, 2], dtype=np.float)
        test_path = 'sample_datasets/temporary_test_results/temporary_test_file_1.bedgraph'
        ref_path = 'sample_datasets/sample_results/result_2-1.bedgraph'
        save_array_as_bedgraph(array, chr_id='chr1', resolution=1, outfile=test_path[:-9])
        self.assertTrue(filecmp.cmp(test_path, ref_path, shallow=False))

    def test_save_as_bedgraph_2(self):
        array = np.array([2, 4, 6, 7, 7, 7, 7, 6, 4, 2], dtype=np.float)
        test_path = 'sample_datasets/temporary_test_results/temporary_test_file_2.bedgraph'
        ref_path = 'sample_datasets/sample_results/result_2-2.bedgraph'
        save_array_as_bedgraph(array, chr_id='chr1', resolution=1000, outfile=test_path[:-9])
        self.assertTrue(filecmp.cmp(test_path, ref_path, shallow=False))



if __name__ == '__main__':
    unittest.main()
