import unittest

from caller_utils import parse_si


class TestCcdCallerUtils(unittest.TestCase):
    def test_parse_si(self):
        self.assertEqual(10, parse_si('10'))
        self.assertEqual(10_000, parse_si('10k'))
        self.assertEqual(10_000_000, parse_si('10M'))
        self.assertEqual(10_000_000_000, parse_si('10G'))
        with self.assertRaises(SystemExit):
            parse_si('dupa')


if __name__ == '__main__':
    unittest.main()
