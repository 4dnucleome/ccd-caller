#!/usr/bin/env python3

import argparse
from collections import namedtuple
from typing import List

from caller_utils import read_interactions_from_bedpe, save_bedgraph
from datatypes import BedGraphRecord

AnchorDiff = namedtuple('AnchorDiff', ['pos', 'diff'])


def get_anchors_diffs(records: List[BedGraphRecord]) -> List[AnchorDiff]:
    """Helper function: calculates how coverage will change at each anchor."""
    anchors = []
    for i in records:
        anchors.append(AnchorDiff(i.begin, i.value))
        anchors.append(AnchorDiff(i.end + 1, -i.value))
    anchors.sort(key=lambda x: x.pos)
    return anchors


def observed_coverage(chr_id: str, records: List[BedGraphRecord]) -> List[BedGraphRecord]:
    """Calculates observed coverage."""
    anchors_diffs = get_anchors_diffs(records)
    coverage = anchors_diffs[0].diff
    start = anchors_diffs[0].pos
    bed_graph_records: List[BedGraphRecord] = []
    for ad in anchors_diffs[1:]:
        bed_graph_records.append(BedGraphRecord(chr_id, start - 1, ad.pos - 1, coverage))
        coverage += ad.diff
        start = ad.pos
    return bed_graph_records


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('chr_id', help='chromosome ID. e.g. chr1, chr12, chrX')
    parser.add_argument('infile', help='loops in bedpe file format')
    parser.add_argument('outfile', help='outfile in bedgraph format')
    args = parser.parse_args()

    records = read_interactions_from_bedpe(args.infile)
    records = observed_coverage(args.chr_id, records)
    save_bedgraph(args.outfile, records, color='32,86,50')


if __name__ == '__main__':
    main()
