import unittest
from collections import deque

from caller_utils import bedgraph_reader, fill_zero_bedgraph
from datatypes import BedGraphRecord
from subtractor import diff_two_bedgraphs


class TestSubtractor(unittest.TestCase):

    def test_fill_zero_bedgraphs(self):
        given = deque([
            BedGraphRecord('chr1', 2, 5, 1),
            BedGraphRecord('chr1', 7, 12, 2)])
        expected = deque([
            BedGraphRecord('chr1', 0, 2, 0),
            BedGraphRecord('chr1', 2, 5, 1),
            BedGraphRecord('chr1', 5, 7, 0),
            BedGraphRecord('chr1', 7, 12, 2),
            BedGraphRecord('chr1', 12, 15, 0)])
        actual = fill_zero_bedgraph(given, 15)
        self.assertEqual(expected, actual)

    def test_diff_two_bedgraphs(self):
        minuend = deque(bedgraph_reader('sample_datasets/bedgraph_1.bedgraph'))
        subtrahend = deque(bedgraph_reader('sample_datasets/bedgraph_2.bedgraph'))
        expected_result = [
            BedGraphRecord('chr1', 1, 2, -1),
            BedGraphRecord('chr1', 4, 6, 1),
            BedGraphRecord('chr1', 6, 8, -1),
            BedGraphRecord('chr1', 9, 10, -2)]
        actual_result = diff_two_bedgraphs(minuend, subtrahend, 10)
        self.assertEqual(actual_result, expected_result)
