import unittest

from datatypes import BedpeRecord


class TestDataTypes(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.record = BedpeRecord('chr1', 4, 8, 'chr1', 17, 20, 5)

    def testBedpeRecord_len(self):
        self.assertEqual(13, len(self.record))

    def testBedpeRecord_str(self):
        self.assertEqual('chr1	4	8	chr1	17	20	5', str(self.record))


if __name__ == '__main__':
    unittest.main()
