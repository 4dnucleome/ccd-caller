#!/usr/bin/env python3

import argparse
import sys
from collections import deque
from typing import Deque, Union

from caller_utils import parse_si, read_bedpe
from datatypes import BedpeRecord


def filter_chromosomes(data: Deque[BedpeRecord], chr_id: str) -> Deque[BedpeRecord]:
    """Removes BEDPE records that do not have interactions between the same chromosome."""
    survives = deque()
    while data:
        record = data.popleft()
        if record.chr1 and record.chr2 == chr_id:
            survives.append(record)
    return survives


def filter_pet_counts_lower_than(data: Deque[BedpeRecord], pet_count_bottom_limit: Union[int, float]) -> Deque[BedpeRecord]:
    """Removes BEDPE records with PET-Count lower than given limit."""
    survives = deque()
    while data:
        record = data.popleft()
        if record.value >= pet_count_bottom_limit:
            survives.append(record)
    return survives


def filter_lengths(data: Deque[BedpeRecord], length_bottom_limit: int, length_top_limit: int) -> Deque[BedpeRecord]:
    """Removes BEDPE records that are longer than given limit."""
    survives = deque()
    while data:
        record = data.popleft()
        if length_bottom_limit <= record.length <= length_top_limit:
            survives.append(record)
    return survives


def filter_chiapet(data: Deque[BedpeRecord], chr_id: str = '', pet_count_bottom_limit: int = 0, length_bottom_limit: int = 0, length_top_limit: Union[int, float] = 250000000) -> Deque[BedpeRecord]:
    """Combines several filters into one convenient function."""
    if chr_id:
        data = filter_chromosomes(data, chr_id)
    if pet_count_bottom_limit > 0:
        data = filter_pet_counts_lower_than(data, pet_count_bottom_limit)
    if length_top_limit < float('inf'):
        data = filter_lengths(data, length_bottom_limit, length_top_limit)
    return data


def main():
    parser = argparse.ArgumentParser(description="removes interactions from BEDPE file and prints output to stdout"
                                                 "that are larger or longer than certain threshold.")
    parser.add_argument('bedpe_file', type=argparse.FileType('r'), default=sys.stdin)
    parser.add_argument('-c', '--chr-id', default='', help="Remove interactions different than specified.")
    parser.add_argument('-p', '--pet-count', default=0, type=int, help="remove interactions lower than this value")
    parser.add_argument('-l', '--length_low_limit', default=0,
                        help="remove interactions shorter than this value. (Length is defined as a distance between "
                             "midpoints of anchors). SI multiplier k, M, G are available (ex 3.5M instead of 3500000).")
    parser.add_argument('-i', '--length_high_limit', default='250M',
                        help="remove interactions shorter than this value. (Length is defined as a distance between "
                             "midpoints of anchors). SI multiplier k, M, G are available (ex 3.5M instead of 3500000).")

    parser.add_argument('-v', '--verbose', action='store_true', default=False, help="print number of removed interactions to stderr")
    args = parser.parse_args()

    data = deque(read_bedpe(args.bedpe_file))
    chr_id = args.chr_id
    pet_count_bottom_limit = args.pet_count
    length_bottom_limit = parse_si(args.length_low_limit)
    length_top_limit = parse_si(args.length_high_limit)
    result = filter_chiapet(data, chr_id, pet_count_bottom_limit, length_bottom_limit, length_top_limit)
    for i in result:
        print(i)


if __name__ == '__main__':
    main()
