CCD Caller
==========
Software designed for calling CCDs from ChIA-PET Data.

Description
-----------
For a given set of interactions saved in BEDPE file format, observed and expected coverage are calculated.
Expected coverage is based on the same set of interactions with assumption that all interactions are scattered across chromosome uniformly.
The CCD signal is calculated by subtracting expected coverage by observed coverage.
The negative values represents loop depletion and positive loop enrichment and thus indicates positions of CCDs.

Detailed algorithm description is chapter 5. of this paper: https://www.sciencedirect.com/science/article/pii/S1046202319300532

##### Please Cite
*Kadlof, Michal, Julia Rozycka, and Dariusz Plewczynski. "Spring Model–chromatin modeling tool based on OpenMM." Methods 181 (2020): 62-69.*

Usage
-----
Simple way:

1. open `run.sh` file and adjust params if needed.
2. run `run.sh <bedpe_file>` and wait few minutes.
3. output dir will be generated.

Advanced way: run all steps independently. Sequence of steps is `run.sh`.
Each script have its own description with `-h` option.

Requirements
------------
python 3.7

`pip install -r requirements.txt`

Example data
------------
Example data are stored in `example_data` dir, and comes from [GSM1872886](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM1872886).
Example result is in directory `example output`.

Viewing results
---------------
Recommended software for inspecting results is [IGV](https://software.broadinstitute.org/software/igv/)

Licence
-------
Copyright (c) 2019 Michał Kadlof

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Contact
-------
Author: Michał Kadlof <m.kadlof@mini.pw.edu.pl>

PI: Dariusz Plewczyński <dariuszplewczynski@cent.uw.edu.pl>
  
