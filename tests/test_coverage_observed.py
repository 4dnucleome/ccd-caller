import filecmp
import unittest

from caller_utils import read_interactions_from_bedpe, save_bedgraph
from coverage_observed import observed_coverage
from datatypes import BedGraphRecord


class TestCoverageObserved(unittest.TestCase):
    def test_coverage_observed(self):
        records = read_interactions_from_bedpe('sample_datasets/sample_2.bedpe')
        actual_results = observed_coverage('chr1', records)
        expected_results = [
            BedGraphRecord('chr1', 0, 4, 3),
            BedGraphRecord('chr1', 4, 6, 4),
            BedGraphRecord('chr1', 6, 7, 6),
            BedGraphRecord('chr1', 7, 9, 3),
            BedGraphRecord('chr1', 9, 10, 2)]
        self.assertListEqual(expected_results, actual_results)

    def test_coverage_observed_bedgraph_save(self):
        ref_path = 'sample_datasets/sample_results/result_3-1.bedgraph'
        test_path = 'sample_datasets/temporary_test_results/temporary_test_file_3.bedgraph'
        records = read_interactions_from_bedpe('sample_datasets/sample_2.bedpe')
        actual_results = observed_coverage('chr1', records)
        save_bedgraph(test_path, actual_results, color='32,86,50')
        self.assertTrue(filecmp.cmp(test_path, ref_path, shallow=False))
