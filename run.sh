#!/usr/bin/env bash

# === PARAMETERS SETUP ===

# Set path to python executable
python3="python3"

# Choose for which chromosomes you want to find CCD's
chromosomes="chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 chr9 chr10 chr11 chr12 chr13 chr14 chr15 chr16 chr17 chr18 chr19 chr20 chr21 chr22 chrX"
#chromosomes="chr8"

# Filter interactions based on length (100 kbp - 2 Mbp recommended)
# Set low limit to 0 and high to 250M if you want to filter data.
length_low_limit=0
length_high_limit=250M

# Filter interactions based on PET-Count (3 recommended)
# Value 4 mean that only clusters with value 4 or higher will be included.
# Singletons usually provide a lot of noise, but does not change results significantly.
count_limit=3

# Genome version. Valid values hg19 and hg38
# You can add more in genomes.py file
genome=hg19

# Resolution (1000 is reasonable)
resolution=1000

# Discard CCDs shorter than this value
discard=100000

# Results directory
output_dir="output_${genome}_PET${count_limit}+_LEN${length_low_limit}-${length_high_limit}"

# === END OF PARAMETERS SETUP ===
# Do NOT edit following unless you know what you are doing!

set -e

inputBedpeFile=$1

echo Input file: ${inputBedpeFile}
echo Using `${python3} -V`

mkdir -p ${output_dir}

echo "=== Splitting input files into separate chromosomes and filtering data based on interaction length and PET-Count ==="
echo "    interaction length: ${length_low_limit}"
echo "    PET-Count limit:    ${count_limit}"
echo -n "    "
for i in ${chromosomes}; do
    echo -n "${i} "
    ${python3} filter_chiapet.py \
               --chr-id ${i} \
               --pet-count ${count_limit} \
               --length_low_limit ${length_low_limit} \
               --length_high_limit ${length_high_limit} \
               ${inputBedpeFile} > ${output_dir}/interactions_${i}_PET_${count_limit}_len_${length_low_limit}.bedpe
done
cat ${output_dir}/interactions_chr* > ${output_dir}/interactions_all_PET_${count_limit}_len_${length_low_limit}.bedpe
echo

echo "=== Calculating expected coverage==="
echo -n "    "
for i in ${chromosomes}; do
    echo -n "${i} "
    ${python3} coverage_expected.py \
               -b \
               --genome ${genome} \
               --outfile ${output_dir}/expected_coverage_${i}_${resolution}_${genome} \
               ${i} \
               ${resolution} \
               ${output_dir}/interactions_${i}_PET_${count_limit}_len_${length_low_limit}.bedpe
done
expected_coverage_fname=expected_coverage_all_${resolution}_${genome}.bedgraph
cat ${output_dir}/expected_coverage_*.bedgraph | grep -v track > ${output_dir}/${expected_coverage_fname}
sed -i "1s/^/track type=bedGraph name=\"${expected_coverage_fname}\" description=\"${expected_coverage_fname}\" color=0,200,0 altColor=200,0,0\n/" ${output_dir}/${expected_coverage_fname}

echo


echo "=== Calculating observed coverage ==="
echo -n "    "
for i in ${chromosomes}; do
    echo -n "${i} "
    ${python3} coverage_observed.py ${i} \
                                    ${output_dir}/interactions_${i}_PET_${count_limit}_len_${length_low_limit}.bedpe \
                                    ${output_dir}/observed_coverage_${i}_${resolution}_${genome}.bedgraph
done

observed_coverage_fname=observed_coverage_all_${resolution}_${genome}.bedgraph
cat ${output_dir}/observed_coverage_* | grep -v track > ${output_dir}/observed_coverage_all_${resolution}_${genome}.bedgraph
sed -i "1s/^/track type=bedGraph name=\"${observed_coverage_fname}\" description=\"${observed_coverage_fname}\" color=0,200,0 altColor=200,0,0\n/" ${output_dir}/${observed_coverage_fname}
echo

echo "=== Calculating CCD signal ==="
echo -n "    "
for i in ${chromosomes}; do
    echo -n "${i} "
    ${python3} subtractor.py ${genome} \
                             ${output_dir}/observed_coverage_${i}_${resolution}_${genome}.bedgraph \
                             ${output_dir}/expected_coverage_${i}_${resolution}_${genome}.bedgraph \
                             ${output_dir}/ccd_signal_${i}_${resolution}_${genome}.bedgraph
done

ccd_signal_fname=ccd_signal_all_${resolution}_${genome}.bedgraph
cat ${output_dir}/ccd_signal_* | grep -v track > ${output_dir}/${ccd_signal_fname}
sed -i "1s/^/track type=bedGraph name=\"${ccd_signal_fname}\" description=\"${ccd_signal_fname}\" color=0,200,0 altColor=200,0,0\n/" ${output_dir}/${ccd_signal_fname}
echo

${python3} ccd_caller.py -d ${discard} ${genome} ${output_dir}/ccd_signal_all_${resolution}_${genome}.bedgraph ${output_dir}/ccds_all_${genome}.bed
cd ${output_dir}
ls | grep -v all | xargs rm
cd ..
echo "Finished"
