#!/usr/bin/env python3

import argparse
from typing import List

import numpy as np
from numba import jit
from tqdm import tqdm

import genomes
from caller_utils import read_bedpe, save_array_as_npz, save_array_as_bedgraph
from datatypes import BedpeRecord


@jit(parallel=True)
def cov_s(coverage: np.ndarray, s_len: int, pet_count: float) -> np.ndarray:
    """Calculates expected coverage over chromosome for one segment.
        Segment length must be already divided by resolution. eg. For segment of length 100kbp in resolution 10kbp you
        should pass s_len == 10.
    """
    chr_len = len(coverage)
    scalar = (pet_count / (chr_len - s_len + 1))  # normalization factor

    # First part
    for i in range(s_len - 1):
        coverage[i] += (i + 1) * scalar

    # Mid part
    coverage[s_len - 1:chr_len - s_len + 1] += s_len * scalar

    # Last part
    for i in range(s_len - 1):
        coverage[chr_len - i - 1] += (i + 1) * scalar
    return coverage


def calculate_coverage(chr_len: int, data: List[BedpeRecord], resolution: int) -> np.ndarray:
    """Calculates expected coverage over chromosome for set of segments."""
    theoretical_coverage = np.zeros(chr_len // resolution, dtype=np.float32)
    for s in tqdm(data):
        theoretical_coverage = cov_s(theoretical_coverage, s.length // resolution, s.value)
    return theoretical_coverage


def main():
    parser = argparse.ArgumentParser(description="Calculate expected coverage by ChIA-PET args (i.e. coverage by randomized interactions positions)")
    parser.add_argument('chromosome', help="name of chromosome. e.g. chr1, chr20, chrX, etc.")
    parser.add_argument('resolution', type=int, help="100 is reasonable.")
    parser.add_argument('infile', help="BEDPE file with ChIA-PET interactions")
    parser.add_argument('-g', '--genome', default="hg38", help="hg19 or hg38. Default is hg38")
    parser.add_argument('-o', '--outfile', help="custom outfile name. File should have no extension")
    parser.add_argument('-b', '--bedgraph', action='store_true', help="Save result as bedgraph file")
    parser.add_argument('-n', '--npz', action='store_true', help="Save result npz file")
    args = parser.parse_args()

    infile = args.infile
    if args.outfile:
        outfile = args.outfile
    else:
        outfile = f"expected_coverage_{args.chromosome}_res{args.resolution}_{args.genome}.npz"
    genome = getattr(genomes, args.genome)

    resolution = args.resolution

    chr_len = genome[args.chromosome]

    with open(infile) as f:
        data: List[BedpeRecord] = read_bedpe(f)

    result = calculate_coverage(chr_len, data, resolution)
    if args.npz:
        save_array_as_npz(result, outfile)
    if args.bedgraph:
        save_array_as_bedgraph(result, args.chromosome, args.resolution, outfile)


if __name__ == '__main__':
    main()
