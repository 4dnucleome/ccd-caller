from dataclasses import field, dataclass
from typing import Union


@dataclass(frozen=True, eq=True)
class BedRecord(object):
    chr_id: str
    begin: int
    end: int
    length: int = field(init=False)

    def __post_init__(self):
        object.__setattr__(self, 'length', self.end - self.begin)

    def __len__(self):
        return self.length

    def __str__(self):
        return f'{self.chr_id}\t{self.begin}\t{self.end}'


@dataclass(frozen=True, eq=True)
class BedpeRecord(object):
    chr1: str
    begin1: int
    end1: int
    chr2: str
    begin2: int
    end2: int
    value: Union[int, float]
    length: int = field(init=False)

    def __post_init__(self):
        a = (self.begin1 + self.end1) // 2
        b = (self.begin2 + self.end2) // 2
        object.__setattr__(self, 'length', b - a + 1)

    def covert_to_interaction(self):
        a = (self.begin1 + self.end1) // 2
        b = (self.begin2 + self.end2) // 2
        return BedGraphRecord(chr_id=self.chr1, begin=a, end=b, value=self.value)

    def __len__(self):
        return self.length

    def __str__(self):
        return f'{self.chr1}\t{self.begin1}\t{self.end1}\t{self.chr2}\t{self.begin2}\t{self.end2}\t{self.value}'


@dataclass(frozen=True, eq=True)
class BedGraphRecord(object):
    """Bedgraph record"""
    chr_id: str
    begin: int
    end: int
    value: Union[int, float]
    length: int = field(init=False)

    def __post_init__(self):
        object.__setattr__(self, 'length', self.end - self.begin)

    def __len__(self):
        return self.length

    def __str__(self):
        return f'{self.chr_id}\t{self.begin}\t{self.end}\t{self.value}'


@dataclass
class MutableBedGraphRecord(object):
    """Simplified not frozen Bedgraph record"""
    chr_id: str
    begin: int
    end: int
    value: Union[int, float]

    def __len__(self):
        return self.end - self.begin + 1

    def __str__(self):
        return f'{self.chr_id}\t{self.begin}\t{self.end}\t{self.value}'
