import csv
import re
import sys
from collections import deque
from typing import List, Dict, Deque

import numpy as np
from typing.io import TextIO

from datatypes import BedpeRecord, MutableBedGraphRecord, BedGraphRecord


def parse_si(s: str) -> int:
    m = re.match(r"^(?P<value>\d*[.,]?\d*)(?P<suffix>[kMG])?$", s)
    if m:
        v = float(m.group('value'))
        suffix = m.group('suffix')
        if suffix == 'k':
            return int(1000 * v)
        elif suffix == "M":
            return int(1e6 * v)
        elif suffix == "G":
            return int(1e9 * v)
        else:
            return int(v)
    else:
        sys.exit(1)


def read_bedpe(f: TextIO) -> List[BedpeRecord]:
    data = []
    with f:
        reader = csv.reader(f, delimiter='\t')
        for i in reader:
            c1, b1, e1, c2, b2, e2, v = i
            b1, e1, b2, e2, v = [int(i) for i in (b1, e1, b2, e2, v)]
            r = BedpeRecord(c1, b1, e1, c2, b2, e2, v)
            data.append(r)
    return data


def save_array_as_npz(array: np.ndarray, outfile: str):
    outfile += '.npz'
    np.savez_compressed(outfile, array)
    print(f'{outfile} saved...')


def save_array_as_bedgraph(array: np.ndarray, chr_id: str, resolution: int, outfile: str):
    """Definition of bedgraph file: http://genome.ucsc.edu/goldenPath/help/bedgraph.html"""
    outfile += '.bedgraph'
    color = '200,200,0'
    alt_color = '200,0,0'
    w = f'track type=bedGraph name="{outfile}" description="{outfile}" color={color} altColor={alt_color}\n'
    records = deque()
    values = deque(array)
    begin = 0
    prev_value = values.popleft()
    record_candidate = MutableBedGraphRecord(chr_id=chr_id, begin=begin, end=begin + resolution, value=prev_value)
    while values:
        next_value = values.popleft()
        if prev_value != next_value:
            records.append(record_candidate)
            record_candidate = MutableBedGraphRecord(chr_id=chr_id, begin=records[-1].end, end=records[-1].end + resolution, value=next_value)
        else:
            record_candidate.end += resolution
        prev_value = next_value
    records.append(record_candidate)

    while records:
        w += str(records.popleft()) + '\n'
    w = w[:-1]
    with open(outfile, 'w') as f:
        f.write(w)
    print(f'{outfile} saved...')


def save_bedgraph(outfile: str, records: List[BedGraphRecord], color='200,200,0', alt_color='200,0,0'):
    w = f'track type=bedGraph name="{outfile}" description="{outfile}" color={color} altColor={alt_color}\n'
    for i in records:
        w += str(i) + '\n'
    w = w[:-1]
    with open(outfile, 'w') as f:
        f.write(w)
    print(f'{outfile} saved.')


def read_interactions_from_bedpe(infile: str) -> List[BedGraphRecord]:
    """Reads BEDPE, but converts it into 3-columns BEDGraph records."""
    with open(infile) as f:
        records = deque(read_bedpe(f))
    interactions = []
    while records:
        r = records.popleft()
        interactions.append(r.covert_to_interaction())
    return interactions


def bedgraph_reader(infile: str) -> List[BedGraphRecord]:
    """Returns list of bedgraph records"""
    data = []
    with open(infile) as f:
        next(f)
        reader = csv.reader(f, delimiter='\t')
        for i in reader:
            chr_id, start, end, value = i
            start, end = [int(i) for i in (start, end)]
            value = float(value)
            r = BedGraphRecord(chr_id, start, end, value)
            data.append(r)
        return data


def bedgraph_reader_as_dict(infile: str, genome: Dict[str, int]) -> Dict[str, List[BedGraphRecord]]:
    """Read bedgraphs with records from more than one chromosome, and return convenient dict"""
    raw_records = deque(bedgraph_reader(infile))
    records = {k: [] for k in genome.keys()}
    while raw_records:
        r = raw_records.popleft()
        records[r.chr_id].append(r)
    return records


def fill_zero_bedgraph(records: Deque[BedGraphRecord], length: int) -> Deque[BedGraphRecord]:
    """Sometimes it's easier to deal with continuous Bedgraph with records with value 0, than with gaps.
    This function fills gaps and borders w ith records with value 0. Records must be sorted, and come from the same chromosome."""
    if not records:
        return deque()
    records = deque(records)
    chr_id = records[0].chr_id
    clean_records = deque()
    r = records.popleft()
    if r.begin != 0:
        clean_records.append(BedGraphRecord(chr_id=chr_id, begin=0, end=r.begin, value=0))
    clean_records.append(r)
    while records:
        r = records.popleft()
        if r.begin != clean_records[-1].end:
            clean_records.append(BedGraphRecord(chr_id=chr_id, begin=clean_records[-1].end, end=r.begin, value=0))
        clean_records.append(r)
    if clean_records[-1].end != length:
        clean_records.append(BedGraphRecord(chr_id=chr_id, begin=clean_records[-1].end, end=length, value=0))
    return clean_records
