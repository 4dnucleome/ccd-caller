import unittest
from collections import deque

from caller_utils import read_bedpe
from datatypes import BedpeRecord
from filter_chiapet import filter_chromosomes, filter_pet_counts_lower_than, filter_lengths_longer_than, filter_chiapet


class TestFilterChiapet(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        with open('sample_datasets/sample_1.bedpe') as f:
            cls.fixed_sample_1 = deque(read_bedpe(f))
        with open('sample_datasets/sample_results/result_1-1.bedpe') as f:
            cls.result_1_1 = deque(read_bedpe(f))
        with open('sample_datasets/sample_results/result_1-2.bedpe') as f:
            cls.result_1_2 = deque(read_bedpe(f))
        with open('sample_datasets/sample_results/result_1-3.bedpe') as f:
            cls.result_1_3 = deque(read_bedpe(f))

    def setUp(self) -> None:
        self.sample_1 = self.fixed_sample_1.copy()

    def test_filter_chromosomes(self):
        result = filter_chromosomes(self.sample_1, 'chr1')
        self.assertEqual(3, len(result))
        self.assertEqual(self.result_1_1, result)

    def test_filter_pet_counts_lower_than(self):
        result = filter_pet_counts_lower_than(self.sample_1, 8)
        self.assertEqual(4, len(result))
        self.assertEqual(self.result_1_2, result)

    def test_filter_lengths_longer_than_1(self):
        result = filter_lengths_longer_than(self.sample_1, 1000)
        self.assertEqual(4, len(result))
        self.assertEqual(self.result_1_3, result)

    def test_filter_lengths_longer_than_2(self):
        sample = deque([BedpeRecord('chr1', 4, 8, 'chr1', 17, 20, 4)])
        result_1 = filter_lengths_longer_than(sample, 13)
        self.assertEqual(1, len(result_1))
        result_2 = filter_lengths_longer_than(sample, 14)
        self.assertEqual(0, len(result_2))

    def test_filter_chiapet_1(self):
        actual_result = filter_chiapet(self.sample_1, 'chr1', 8, 1000)
        expected_result = deque([BedpeRecord('chr1', 100, 105, 'chr1', 200, 205, 8)])
        self.assertEqual(1, len(actual_result))
        self.assertEqual(expected_result, actual_result)

    def test_filter_chiapet_2(self):
        actual_result = filter_chiapet(self.sample_1, pet_count_bottom_limit=8, length_top_limit=1000)
        expected_result = deque([BedpeRecord('chr1', 100, 105, 'chr1', 200, 205, 8),
                                 BedpeRecord('chr1', 30, 35, 'chr2', 60, 65, 12),
                                 BedpeRecord('chr3', 70, 75, 'chr3', 150, 155, 13)])
        self.assertEqual(3, len(actual_result))
        self.assertEqual(expected_result, actual_result)

    def test_filter_chiapet_3(self):
        actual_result = filter_chiapet(self.sample_1, chr_id='chr1', length_top_limit=1000)
        expected_result = deque([BedpeRecord('chr1', 100, 105, 'chr1', 200, 205, 8),
                                 BedpeRecord('chr1', 80, 85, 'chr1', 300, 305, 2)])
        self.assertEqual(2, len(actual_result))
        self.assertEqual(expected_result, actual_result)

    def test_filter_chiapet_4(self):
        actual_result = filter_chiapet(self.sample_1, chr_id='chr1', pet_count_bottom_limit=8)
        expected_result = deque([BedpeRecord('chr1', 100, 105, 'chr1', 200, 205, 8),
                                 BedpeRecord('chr1', 200, 205, 'chr1', 10000, 10005, 10)])
        self.assertEqual(2, len(actual_result))
        self.assertEqual(expected_result, actual_result)


if __name__ == '__main__':
    unittest.main()
