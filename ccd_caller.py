import argparse
from typing import List, Union, Dict

import genomes
from caller_utils import bedgraph_reader
from datatypes import BedRecord, BedGraphRecord


def sign(x: Union[float, int]) -> bool:
    return x >= 0


def call_ccds(records: List[BedGraphRecord], chromosomes: List[str]) -> Dict[str, List[BedRecord]]:
    records_by_chromosomes = {k: [] for k in chromosomes}
    ccds_by_chromosomes = {k: [] for k in chromosomes}
    for r in records:
        records_by_chromosomes[r.chr_id].append(r)

    for chr_id in chromosomes:
        records = records_by_chromosomes[chr_id]
        if len(records) > 0:
            current_sign = sign(records[0].value)
            current_begin = records[0].begin
            current_end = records[0].end

            for r in records[1:]:
                # +/+ or -/-
                if current_sign == sign(r.value):
                    current_end = r.end
                # +/-
                elif current_sign and not sign(r.value):
                    ccds_by_chromosomes[chr_id].append(BedRecord(r.chr_id, current_begin, current_end))
                    current_sign = sign(r.value)
                # -/+
                elif not current_sign and sign(r.value):
                    current_begin = r.begin
                    current_end = r.end
                    current_sign = sign(r.value)
        else:
            del ccds_by_chromosomes[chr_id]
    return ccds_by_chromosomes


def some_stats(ccds: List[BedRecord]):
    print(f'Number of CCDs: {len(ccds)}')
    print(f'Mean CCD length: {sum(len(i) for i in ccds) / len(ccds):0.0f}')


def save_bed(ccds: List[BedRecord], outfile: str) -> None:
    w = ''
    for r in ccds:
        w += str(r) + '\n'
    w = w[:-1]
    with open(outfile, 'w') as f:
        f.write(w)
    print(f'{outfile} saved.')


def main():
    parser = argparse.ArgumentParser(description="Calls CCD's from CCD signal bedgraph files")
    parser.add_argument('-d', '--discard', default=0, type=int, help='discard CCDs shorter than this value.')
    parser.add_argument('genome', help="genome: hg19 or hg38")
    parser.add_argument('infile', help='ccd signal bedgraph file')
    parser.add_argument('outfile', help='BED file with ccds')
    args = parser.parse_args()

    genome = getattr(genomes, args.genome)
    chromosomes = sorted(genome.keys())

    records = bedgraph_reader(args.infile)
    ccds = call_ccds(records, chromosomes)
    ccds = sorted([r for chr_id in ccds.keys() for r in ccds[chr_id]], key=lambda x: chromosomes.index(x.chr_id))
    ccds = list(filter(lambda x: x.length > args.discard, ccds))
    save_bed(ccds, args.outfile)
    some_stats(ccds)


if __name__ == '__main__':
    main()
