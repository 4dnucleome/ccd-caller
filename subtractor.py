#!/usr/bin/env python3

import argparse
from collections import namedtuple, deque
from typing import List, Deque, Dict, Union

import genomes
from caller_utils import save_bedgraph, bedgraph_reader_as_dict, fill_zero_bedgraph
from datatypes import BedGraphRecord


def diff_two_bedgraphs(minuend: Union[List[BedGraphRecord], Deque[BedGraphRecord]],
                       subtrahend: Union[List[BedGraphRecord], Deque[BedGraphRecord]],
                       chr_length: int) -> List[BedGraphRecord]:
    """Subtracts one bedgraph from another with bp resolution.
    All records must come from the same chromosome."""

    # Is there really anything to do?
    if not minuend or not subtrahend:
        return []

    # Ok, let's do something useful
    ChangePoint = namedtuple('ChangePoint', ['pos', 'track', 'value', 'diff'])

    # Prepare bedgraphs
    if type(minuend) == list:
        minuend: Deque = deque(minuend)
    if type(subtrahend) == list:
        subtrahend: Deque = deque(subtrahend)
    minuend = fill_zero_bedgraph(minuend, chr_length)
    subtrahend = fill_zero_bedgraph(subtrahend, chr_length)
    chr_id = minuend[0].chr_id

    # prepare change points
    change_points: List[ChangePoint] = []
    r0 = minuend.popleft()
    change_points.append(ChangePoint(pos=r0.begin + 1, track=1, value=r0.value, diff=None))
    while minuend:
        r = minuend.popleft()
        change_points.append(ChangePoint(pos=r.begin + 1, track=1, value=r.value, diff=r.value - change_points[-1].value))

    r0 = subtrahend.popleft()
    change_points.append(ChangePoint(pos=r0.begin + 1, track=2, value=-r0.value, diff=None))
    while subtrahend:
        r = subtrahend.popleft()
        change_points.append(ChangePoint(pos=r.begin + 1, track=2, value=-r.value, diff=-r.value - change_points[-1].value))

    change_points.sort(key=lambda x: x.pos)
    change_points: Deque[ChangePoint] = deque(change_points)

    # Do something smart
    result: Deque[BedGraphRecord] = deque()
    current_value = change_points[0].value - change_points[1].value
    change_points.popleft()
    change_points.popleft()
    begin = 0
    while change_points:
        r = change_points.popleft()
        result.append(BedGraphRecord(chr_id=chr_id, begin=begin, end=r.pos - 1, value=current_value))
        begin = r.pos - 1
        current_value += r.diff
    result.append(BedGraphRecord(chr_id=chr_id, begin=begin, end=chr_length, value=current_value))
    clean_result = []
    while result:
        r = result.popleft()
        if r.value != 0:
            clean_result.append(r)
    return clean_result


def main():
    parser = argparse.ArgumentParser(description="Subtracts two bedgraph files.")
    parser.add_argument('genome', default='hg38', help="hg19 or hg38 (Default: hg38)")
    parser.add_argument('minuend', help="First bedgraph file")
    parser.add_argument('subtrahend', help="Second bedgraph file")
    parser.add_argument('outfile', help="normalized coverage (bedgraph file)")
    parser.add_argument('-d', '--description', default='', help="description added to header of output bedgraph file")
    args = parser.parse_args()

    genome: Dict[str, int] = getattr(genomes, args.genome)

    minuend_dict = bedgraph_reader_as_dict(args.minuend, genome)
    subtrahend_dict = bedgraph_reader_as_dict(args.subtrahend, genome)

    diff_all = []
    for chr_id in genome.keys():
        minuend = deque(minuend_dict[chr_id])
        subtrahend = deque(subtrahend_dict[chr_id])
        difference = diff_two_bedgraphs(minuend, subtrahend, chr_length=genome[chr_id])
        diff_all += difference
    save_bedgraph(args.outfile, diff_all, color='0,200,0')


if __name__ == '__main__':
    main()
